# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :src,
  ecto_repos: [Src.Repo]

# Configures the endpoint
config :src, SrcWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "EU/66FpkcVAfbGw4IDho+s/Ck0oZyxix3BDOQvP6Fe+dHj1xg1Rz6qr6+VxNI96Q",
  render_errors: [view: SrcWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Src.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
