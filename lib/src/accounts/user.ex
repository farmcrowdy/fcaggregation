defmodule Src.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias Src.Accounts.Encryption

  schema "users" do

    field :firstname, :string
    field :surname, :string
    field :email, :string
    field :password, :string
    field :uphone, :string
    field :dob, :string
    field :approved, :integer
    field :gender, :string
    field :role, :string
    field :pin, :integer
    field :hasFCMBWallet, :integer

    belongs_to :client, Src.Accounts.Client
    belongs_to :access_level, Src.Accounts.Access_level

    ##Virtual Fields ##
    field :passwordfield, :string, virtual: true
    field :passwordfield_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:firstname, :surname, :email, :passwordfield, :passwordfield_confirmation, :client_id])
    |> validate_required([:firstname, :surname, :email, :passwordfield, :passwordfield_confirmation, :client_id])
    |> validate_format(:email, ~r/@/)
    |> update_change(:email, &String.downcase(&1))
    |> validate_length(:passwordfield, min: 6, max: 100)
    |> validate_confirmation(:passwordfield)
    |> unique_constraint(:email)
    |> encrypt_password
  end

  defp encrypt_password(changeset) do
    password = get_change(changeset, :passwordfield)

    if password do
      encrypted_password = Encryption.hash_password(password).password_hash

      put_change(changeset, :password, encrypted_password)

    else
      changeset
    end

  end
end
