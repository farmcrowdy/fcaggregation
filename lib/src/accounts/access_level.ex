defmodule Src.Accounts.Access_level do
  use Ecto.Schema
  import Ecto.Changeset

  schema "access_levels" do
    field :age, :integer
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(access_level, attrs) do
    access_level
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
