defmodule Src.Farms do
  @moduledoc """
  The Farms context.
  """

  import Ecto.Query, warn: false
  alias Src.Repo

  alias Src.Farms.Farmer

  @doc """
  Returns the list of farmers.

  ## Examples

      iex> list_farmers()
      [%Farmer{}, ...]

  """
  def list_farmers do
    Repo.all(Farmer)
  end

  @doc """
  Gets a single farmer.

  Raises `Ecto.NoResultsError` if the Farmer does not exist.

  ## Examples

      iex> get_farmer!(123)
      %Farmer{}

      iex> get_farmer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_farmer(id), do: Repo.get(Farmer, id)

  @doc """
  Creates a farmer.

  ## Examples

      iex> create_farmer(%{field: value})
      {:ok, %Farmer{}}

      iex> create_farmer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_farmer(attrs \\ %{}) do
    %Farmer{}
    |> Farmer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a farmer.

  ## Examples

      iex> update_farmer(farmer, %{field: new_value})
      {:ok, %Farmer{}}

      iex> update_farmer(farmer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_farmer(%Farmer{} = farmer, attrs) do
    farmer
    |> Farmer.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a farmer.

  ## Examples

      iex> delete_farmer(farmer)
      {:ok, %Farmer{}}

      iex> delete_farmer(farmer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_farmer(%Farmer{} = farmer) do
    Repo.delete(farmer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking farmer changes.

  ## Examples

      iex> change_farmer(farmer)
      %Ecto.Changeset{source: %Farmer{}}

  """
  def change_farmer(%Farmer{} = farmer) do
    Farmer.changeset(farmer, %{})
  end

  alias Src.Farms.Project

  @doc """
  Returns the list of projects.

  ## Examples

      iex> list_projects()
      [%Project{}, ...]

  """
  def list_projects do
    Repo.all(Project)
  end

  @doc """
  Gets a single project.

  Raises `Ecto.NoResultsError` if the Project does not exist.

  ## Examples

      iex> get_project!(123)
      %Project{}

      iex> get_project!(456)
      ** (Ecto.NoResultsError)

  """
  def get_project!(id), do: Repo.get!(Project, id)

  @doc """
  Creates a project.

  ## Examples

      iex> create_project(%{field: value})
      {:ok, %Project{}}

      iex> create_project(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_project(attrs \\ %{}) do
    %Project{}
    |> Project.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a project.

  ## Examples

      iex> update_project(project, %{field: new_value})
      {:ok, %Project{}}

      iex> update_project(project, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_project(%Project{} = project, attrs) do
    project
    |> Project.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a project.

  ## Examples

      iex> delete_project(project)
      {:ok, %Project{}}

      iex> delete_project(project)
      {:error, %Ecto.Changeset{}}

  """
  def delete_project(%Project{} = project) do
    Repo.delete(project)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking project changes.

  ## Examples

      iex> change_project(project)
      %Ecto.Changeset{source: %Project{}}

  """
  def change_project(%Project{} = project) do
    Project.changeset(project, %{})
  end

  alias Src.Farms.State

  @doc """
  Returns the list of states.

  ## Examples

      iex> list_states()
      [%State{}, ...]

  """
  def list_states do
    Repo.all(State)
  end

  @doc """
  Gets a single state.

  Raises `Ecto.NoResultsError` if the State does not exist.

  ## Examples

      iex> get_state!(123)
      %State{}

      iex> get_state!(456)
      ** (Ecto.NoResultsError)

  """
  def get_state!(id), do: Repo.get!(State, id)

  @doc """
  Creates a state.

  ## Examples

      iex> create_state(%{field: value})
      {:ok, %State{}}

      iex> create_state(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_state(attrs \\ %{}) do
    %State{}
    |> State.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a state.

  ## Examples

      iex> update_state(state, %{field: new_value})
      {:ok, %State{}}

      iex> update_state(state, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_state(%State{} = state, attrs) do
    state
    |> State.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a state.

  ## Examples

      iex> delete_state(state)
      {:ok, %State{}}

      iex> delete_state(state)
      {:error, %Ecto.Changeset{}}

  """
  def delete_state(%State{} = state) do
    Repo.delete(state)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking state changes.

  ## Examples

      iex> change_state(state)
      %Ecto.Changeset{source: %State{}}

  """
  def change_state(%State{} = state) do
    State.changeset(state, %{})
  end

  alias Src.Farms.Local

  @doc """
  Returns the list of locals.

  ## Examples

      iex> list_locals()
      [%Local{}, ...]

  """
  def list_locals do
    Repo.all(Local)
  end

  @doc """
  Gets a single local.

  Raises `Ecto.NoResultsError` if the Local does not exist.

  ## Examples

      iex> get_local!(123)
      %Local{}

      iex> get_local!(456)
      ** (Ecto.NoResultsError)

  """
  def get_local!(id), do: Repo.get!(Local, id)

  @doc """
  Creates a local.

  ## Examples

      iex> create_local(%{field: value})
      {:ok, %Local{}}

      iex> create_local(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_local(attrs \\ %{}) do
    %Local{}
    |> Local.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a local.

  ## Examples

      iex> update_local(local, %{field: new_value})
      {:ok, %Local{}}

      iex> update_local(local, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_local(%Local{} = local, attrs) do
    local
    |> Local.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a local.

  ## Examples

      iex> delete_local(local)
      {:ok, %Local{}}

      iex> delete_local(local)
      {:error, %Ecto.Changeset{}}

  """
  def delete_local(%Local{} = local) do
    Repo.delete(local)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking local changes.

  ## Examples

      iex> change_local(local)
      %Ecto.Changeset{source: %Local{}}

  """
  def change_local(%Local{} = local) do
    Local.changeset(local, %{})
  end

  alias Src.Farms.Farmgroup

  @doc """
  Returns the list of farm_group.

  ## Examples

      iex> list_farm_group()
      [%Farmgroup{}, ...]

  """
  def list_farm_group do
    Repo.all(Farmgroup)
  end

  @doc """
  Gets a single farmgroup.

  Raises `Ecto.NoResultsError` if the Farmgroup does not exist.

  ## Examples

      iex> get_farmgroup!(123)
      %Farmgroup{}

      iex> get_farmgroup!(456)
      ** (Ecto.NoResultsError)

  """
  def get_farmgroup!(id), do: Repo.get!(Farmgroup, id)

  @doc """
  Creates a farmgroup.

  ## Examples

      iex> create_farmgroup(%{field: value})
      {:ok, %Farmgroup{}}

      iex> create_farmgroup(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_farmgroup(attrs \\ %{}) do
    %Farmgroup{}
    |> Farmgroup.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a farmgroup.

  ## Examples

      iex> update_farmgroup(farmgroup, %{field: new_value})
      {:ok, %Farmgroup{}}

      iex> update_farmgroup(farmgroup, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_farmgroup(%Farmgroup{} = farmgroup, attrs) do
    farmgroup
    |> Farmgroup.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a farmgroup.

  ## Examples

      iex> delete_farmgroup(farmgroup)
      {:ok, %Farmgroup{}}

      iex> delete_farmgroup(farmgroup)
      {:error, %Ecto.Changeset{}}

  """
  def delete_farmgroup(%Farmgroup{} = farmgroup) do
    Repo.delete(farmgroup)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking farmgroup changes.

  ## Examples

      iex> change_farmgroup(farmgroup)
      %Ecto.Changeset{source: %Farmgroup{}}

  """
  def change_farmgroup(%Farmgroup{} = farmgroup) do
    Farmgroup.changeset(farmgroup, %{})
  end

  alias Src.Farms.Farmlocation

  @doc """
  Returns the list of farm_location.

  ## Examples

      iex> list_farm_location()
      [%Farmlocation{}, ...]

  """
  def list_farm_location do
    Repo.all(Farmlocation)
  end

  @doc """
  Gets a single farmlocation.

  Raises `Ecto.NoResultsError` if the Farmlocation does not exist.

  ## Examples

      iex> get_farmlocation!(123)
      %Farmlocation{}

      iex> get_farmlocation!(456)
      ** (Ecto.NoResultsError)

  """
  def get_farmlocation!(id), do: Repo.get!(Farmlocation, id)

  @doc """
  Creates a farmlocation.

  ## Examples

      iex> create_farmlocation(%{field: value})
      {:ok, %Farmlocation{}}

      iex> create_farmlocation(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_farmlocation(attrs \\ %{}) do
    %Farmlocation{}
    |> Farmlocation.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a farmlocation.

  ## Examples

      iex> update_farmlocation(farmlocation, %{field: new_value})
      {:ok, %Farmlocation{}}

      iex> update_farmlocation(farmlocation, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_farmlocation(%Farmlocation{} = farmlocation, attrs) do
    farmlocation
    |> Farmlocation.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a farmlocation.

  ## Examples

      iex> delete_farmlocation(farmlocation)
      {:ok, %Farmlocation{}}

      iex> delete_farmlocation(farmlocation)
      {:error, %Ecto.Changeset{}}

  """
  def delete_farmlocation(%Farmlocation{} = farmlocation) do
    Repo.delete(farmlocation)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking farmlocation changes.

  ## Examples

      iex> change_farmlocation(farmlocation)
      %Ecto.Changeset{source: %Farmlocation{}}

  """
  def change_farmlocation(%Farmlocation{} = farmlocation) do
    Farmlocation.changeset(farmlocation, %{})
  end

  alias Src.Farms.Bank

  @doc """
  Returns the list of banks.

  ## Examples

      iex> list_banks()
      [%Bank{}, ...]

  """
  def list_banks do
    Repo.all(Bank)
  end

  @doc """
  Gets a single bank.

  Raises `Ecto.NoResultsError` if the Bank does not exist.

  ## Examples

      iex> get_bank!(123)
      %Bank{}

      iex> get_bank!(456)
      ** (Ecto.NoResultsError)

  """
  def get_bank!(id), do: Repo.get!(Bank, id)

  @doc """
  Creates a bank.

  ## Examples

      iex> create_bank(%{field: value})
      {:ok, %Bank{}}

      iex> create_bank(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_bank(attrs \\ %{}) do
    %Bank{}
    |> Bank.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a bank.

  ## Examples

      iex> update_bank(bank, %{field: new_value})
      {:ok, %Bank{}}

      iex> update_bank(bank, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_bank(%Bank{} = bank, attrs) do
    bank
    |> Bank.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a bank.

  ## Examples

      iex> delete_bank(bank)
      {:ok, %Bank{}}

      iex> delete_bank(bank)
      {:error, %Ecto.Changeset{}}

  """
  def delete_bank(%Bank{} = bank) do
    Repo.delete(bank)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking bank changes.

  ## Examples

      iex> change_bank(bank)
      %Ecto.Changeset{source: %Bank{}}

  """
  def change_bank(%Bank{} = bank) do
    Bank.changeset(bank, %{})
  end
end
