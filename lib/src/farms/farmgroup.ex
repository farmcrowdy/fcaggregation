defmodule Src.Farms.Farmgroup do
  use Ecto.Schema
  import Ecto.Changeset

  schema "farm_group" do
    field :age, :integer
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(farmgroup, attrs) do
    farmgroup
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
