defmodule Src.Farms.Local do
  use Ecto.Schema
  import Ecto.Changeset

  schema "locals" do
    field :age, :integer
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(local, attrs) do
    local
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
