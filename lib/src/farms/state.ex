defmodule Src.Farms.State do
  use Ecto.Schema
  import Ecto.Changeset

  schema "states" do
    field :age, :integer
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(state, attrs) do
    state
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
