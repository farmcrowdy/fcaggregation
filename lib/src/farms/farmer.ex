defmodule Src.Farms.Farmer do
  use Ecto.Schema
  import Ecto.Changeset

  alias Src.Upload

  schema "fcdy_novus" do
    field :fname, :string
    field :sname, :string
    field :bvnNumber, :string
    field :idType, :string
    field :idNumber, :string
    field :farmerId, :string
    field :bankName, :string
    field :govtString, :string
    field :nextOfKin, :string
    field :synced, :string
    field :nextOfKinPhone, :string
    field :role, :string
    field :dob, :string
    field :dobForPaga, :string
    field :gender, :string
    field :f_print, :string
    field :phone, :string
    field :marital_status, :string
    field :crop_proficiency, :string
    field :income_range, :string
    field :pro_image, :string
    field :pro_image_thumbnail, :string
    field :comment, :string
    field :acct_number, :string
    field :land_area_farmed, :string
    field :lat1, :string
    field :lat2, :string
    field :lat3, :string
    field :lat4, :string
    field :haveSmartPhone, :string
    field :long1, :string
    field :long2, :string
    field :long3, :string
    field :long4, :string
    field :number_of_dependants, :integer
    field :years_of_experience, :integer
    field :coordinates, :string
    field :comments, :string
    field :timestamp, :string
    field :uploadedToParent, :string
    field :sourceOfData, :integer
    field :readWriteEnglish, :string
    field :hasFCMBWallet, :integer
    field :fcmb_pin, :integer, default: 0
    field :appVersion, :string

    # Virtual Fields
    field :proimage_field, :string, virtual: true

    belongs_to :client, Src.Accounts.Client
    belongs_to :user_add, Src.Accounts.User
    belongs_to :project, Src.Farms.Project
    belongs_to :state, Src.Farms.State
    belongs_to :local, Src.Farms.Local
    belongs_to :farm_group, Src.Farms.Farmgroup
    belongs_to :farm_location, Src.Farms.Farmlocation
    belongs_to :bank, Src.Farms.Bank


    timestamps()
  end

  @doc false
  def changeset(farmer, attrs) do
    farmer
    |> cast(attrs, [
      :fname, :sname, :bank_id, :client_id, :farm_group_id, :farm_location_id,
      :local_id, :project_id, :state_id, :user_add_id, :bvnNumber, :idType, :idNumber, :farmerId,
      :bankName, :govtString, :nextOfKin, :synced, :nextOfKinPhone, :role, :dob, :dobForPaga, :gender,
      :f_print, :phone, :marital_status, :crop_proficiency, :income_range, :comment, :acct_number,
      :land_area_farmed, :lat1, :lat2, :lat3, :lat4, :haveSmartPhone, :long1, :long2, :long3, :long4,
      :number_of_dependants, :years_of_experience, :coordinates, :comments, :timestamp, :uploadedToParent,
      :sourceOfData, :readWriteEnglish, :hasFCMBWallet, :fcmb_pin, :appVersion
      ])
    |> validate_required([
      :fname, :sname, :bank_id, :client_id, :farm_group_id, :farm_location_id,
      :local_id, :project_id, :state_id, :user_add_id
      ])
    |> upload_oncreation(attrs)
  end

  defp upload_oncreation(changeset, attrs) do
    if attrs["proimage_field"] do

      uploadFileName = Upload.upload_to_buck(attrs["proimage_field"])
      put_change(changeset, :pro_image, uploadFileName)

    else
      put_change(changeset, :pro_image, "farmers-images/1d1f36b8250d4e2ba29500bf7b21dde4-user-avatar.jpg")
    end
  end


end
