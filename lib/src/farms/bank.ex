defmodule Src.Farms.Bank do
  use Ecto.Schema
  import Ecto.Changeset

  schema "banks" do
    field :age, :integer
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(bank, attrs) do
    bank
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
