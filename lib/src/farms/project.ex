defmodule Src.Farms.Project do
  use Ecto.Schema
  import Ecto.Changeset

  schema "projects" do
    field :age, :integer
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(project, attrs) do
    project
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
