defmodule Src.Farms.Farmlocation do
  use Ecto.Schema
  import Ecto.Changeset

  schema "farm_location" do
    field :age, :integer
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(farmlocation, attrs) do
    farmlocation
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
