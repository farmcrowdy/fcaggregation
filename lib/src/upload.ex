defmodule Src.Upload do

  def bucket_name do
    Application.get_env(:src, :BUCKET_NAME)
  end

  def upload_to_buck(%Plug.Upload{
    filename: filename,
    path: tmp_path,
    content_type: _content_type
  }) do
      file_uuid = UUID.uuid4(:hex)
      unique_filename = "farmers-images/#{file_uuid}-#{filename}"
      {:ok, image_binary} = File.read(tmp_path)

        ExAws.S3.put_object(bucket_name(), unique_filename, image_binary)
        |> ExAws.request!

      #newFilename = "https://#{bucket_name()}.s3.amazonaws.com/#{bucket_name()}/#{unique_filename}"

      unique_filename

  end

  def get_uploaded_img(unique_filename) do
    image =
      ExAws.S3.get_object(bucket_name(), unique_filename)
      |> ExAws.request!
      
    image
  end

end
