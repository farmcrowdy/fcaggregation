defmodule Src.Accounts do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias Src.Repo

  alias Src.Accounts.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  alias Src.Accounts.Client

  @doc """
  Returns the list of clients.

  ## Examples

      iex> list_clients()
      [%Client{}, ...]

  """
  def list_clients do
    Repo.all(Client)
  end

  @doc """
  Gets a single client.

  Raises `Ecto.NoResultsError` if the Client does not exist.

  ## Examples

      iex> get_client!(123)
      %Client{}

      iex> get_client!(456)
      ** (Ecto.NoResultsError)

  """
  def get_client!(id), do: Repo.get!(Client, id)

  @doc """
  Creates a client.

  ## Examples

      iex> create_client(%{field: value})
      {:ok, %Client{}}

      iex> create_client(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_client(attrs \\ %{}) do
    %Client{}
    |> Client.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a client.

  ## Examples

      iex> update_client(client, %{field: new_value})
      {:ok, %Client{}}

      iex> update_client(client, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_client(%Client{} = client, attrs) do
    client
    |> Client.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a client.

  ## Examples

      iex> delete_client(client)
      {:ok, %Client{}}

      iex> delete_client(client)
      {:error, %Ecto.Changeset{}}

  """
  def delete_client(%Client{} = client) do
    Repo.delete(client)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking client changes.

  ## Examples

      iex> change_client(client)
      %Ecto.Changeset{source: %Client{}}

  """
  def change_client(%Client{} = client) do
    Client.changeset(client, %{})
  end

  alias Src.Accounts.Access_level

  @doc """
  Returns the list of access_levels.

  ## Examples

      iex> list_access_levels()
      [%Access_level{}, ...]

  """
  def list_access_levels do
    Repo.all(Access_level)
  end

  @doc """
  Gets a single access_level.

  Raises `Ecto.NoResultsError` if the Access level does not exist.

  ## Examples

      iex> get_access_level!(123)
      %Access_level{}

      iex> get_access_level!(456)
      ** (Ecto.NoResultsError)

  """
  def get_access_level!(id), do: Repo.get!(Access_level, id)

  @doc """
  Creates a access_level.

  ## Examples

      iex> create_access_level(%{field: value})
      {:ok, %Access_level{}}

      iex> create_access_level(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_access_level(attrs \\ %{}) do
    %Access_level{}
    |> Access_level.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a access_level.

  ## Examples

      iex> update_access_level(access_level, %{field: new_value})
      {:ok, %Access_level{}}

      iex> update_access_level(access_level, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_access_level(%Access_level{} = access_level, attrs) do
    access_level
    |> Access_level.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a access_level.

  ## Examples

      iex> delete_access_level(access_level)
      {:ok, %Access_level{}}

      iex> delete_access_level(access_level)
      {:error, %Ecto.Changeset{}}

  """
  def delete_access_level(%Access_level{} = access_level) do
    Repo.delete(access_level)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking access_level changes.

  ## Examples

      iex> change_access_level(access_level)
      %Ecto.Changeset{source: %Access_level{}}

  """
  def change_access_level(%Access_level{} = access_level) do
    Access_level.changeset(access_level, %{})
  end
end
