defmodule SrcWeb.Router do
  use SrcWeb, :router

  pipeline :browser do
    plug :accepts, ["html json"]
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SrcWeb do
    pipe_through :browser

    get "/", HomeController, :index
  end

  scope "/api", SrcWeb do
    pipe_through :api

    get "/users", UserController, :index
    get "/user/:id", UserController, :show
    post "/register", UserController, :create_user
    post "/login", UserController, :login

    post "/add-farmer", FarmController, :create_farmer
    get "/farmers", FarmController, :get_farmers
    get "/farmer/:id", FarmController, :get_farmer
    get "/get-image/:id", FarmController, :get_image
  end
end
