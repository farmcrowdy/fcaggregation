defmodule SrcWeb.HomeController do
  use SrcWeb, :controller

  def index(conn, _params) do
    conn
    |> put_status(401)
    |> json(%{message: "farmcrowdy data collection hot code reload test"})
  end

end
