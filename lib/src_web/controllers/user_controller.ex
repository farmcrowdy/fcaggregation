defmodule SrcWeb.UserController do
  use SrcWeb, :controller

  alias Src.{Repo,Accounts}
  alias Src.Accounts.{User, Auth}

  action_fallback SrcWeb.FallbackController

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.json", users: users)
  end

  def create_user(conn, user_params) do
    # IO.puts "+++++++++++++"
    # IO.inspect user_params
    # IO.puts "+++++++++++++"
    with {:ok, %User{} = user} <- Accounts.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_path(conn, :show, user))
      |> render("create.json", user: user)
    end
  end

  def login(conn, login_params) do

    case Auth.login(login_params, Repo) do
      {:ok, user} ->
        conn
        |> put_status(200)
        |> render("login.json", user: user)

      :error ->
        conn
        |> put_status(401)
        |> json(%{status: "Incorrect email or password"})

    end

  end

  # def show(conn, %{"id" => id}) do
  #   user = Accounts.get_user!(id)
  #   render(conn, "show.json", user: user)
  # end
  #
  # def update(conn, %{"id" => id, "user" => user_params}) do
  #   user = Accounts.get_user!(id)
  #
  #   with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
  #     render(conn, "show.json", user: user)
  #   end
  # end
  #
  # def delete(conn, %{"id" => id}) do
  #   user = Accounts.get_user!(id)
  #
  #   with {:ok, %User{}} <- Accounts.delete_user(user) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
