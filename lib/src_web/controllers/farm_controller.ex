defmodule SrcWeb.FarmController do
  use SrcWeb, :controller

  alias Src.{Farms, Upload}
  alias Src.Farms.Farmer

  action_fallback SrcWeb.FallbackController

  def create_farmer(conn, farm_params) do

    with {:ok, %Farmer{} = farmer} <- Farms.create_farmer(farm_params) do
      conn
      |> put_status(:created)
      |> render("create.json", farmer: farmer)
    end

  end

  def get_farmers(conn, _params) do
    farmers = Farms.list_farmers()
    render(conn, "index.json", farmers: farmers)
  end

  def get_farmer(conn, %{"id" => id}) do
    farmer = Farms.get_farmer(id)
    case farmer do
      nil ->
        conn
        |> put_status(:not_found)
        |> json(%{message: "farmer not found"})

      _ -> render(conn, "show.json", farmer: farmer)
    end
  end

  def get_image(conn, %{"id" => id}) do
    farmer = Farms.get_farmer(id)
      case farmer do
        nil ->
          conn
          |> put_status(:not_found)
          |> json(%{message: "farmer not found"})

        _ ->
          image = Upload.get_uploaded_img(farmer.pro_image).body
          conn
          |> put_resp_header("content-type", "image/png")
          |> send_resp(200, image)
      end
  end

end
