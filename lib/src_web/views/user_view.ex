defmodule SrcWeb.UserView do
  use SrcWeb, :view
  alias SrcWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("create.json", %{user: user}) do
    %{message: "User created successfully", data: render_one(user, UserView, "user.json")}
  end

  def render("login.json", %{user: user}) do
    %{message: "User logged in successfully", data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      first_name: user.firstname,
      surname: user.surname,
      email: user.email,
      phone: user.uphone
    }
  end
end
