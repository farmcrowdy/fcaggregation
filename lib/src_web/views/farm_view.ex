defmodule SrcWeb.FarmView do
  use SrcWeb, :view
  alias SrcWeb.FarmView

  def render("index.json", %{farmers: farmers}) do
    %{data: render_many(farmers, FarmView, "farm.json")}
  end

  def render("show.json", %{farmer: farmer}) do
    %{data: render_one(farmer, FarmView, "farm.json")}
  end

  def render("create.json", %{farmer: farmer}) do
    %{message: "farmer created successfully", data: render_one(farmer, FarmView, "farm.json")}
  end

  def render("farm.json", %{farm: farmer}) do
    %{
      firstname: farmer.fname,
      surname: farmer.sname
    }
  end
end
