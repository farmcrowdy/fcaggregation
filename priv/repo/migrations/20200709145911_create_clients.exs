defmodule Src.Repo.Migrations.CreateClients do
  use Ecto.Migration

  def change do
    create table(:clients) do
      add :client_name, :string
      add :address, :text

      timestamps()
    end

  end
end
