defmodule Src.Repo.Migrations.ChangeState do
  use Ecto.Migration

  def change do
    alter table(:states) do
      remove :inserted_at
      remove :updated_at
    end
  end
end
