defmodule Src.Repo.Migrations.CreateProjects do
  use Ecto.Migration

  def change do
    create table(:projects) do
      add :name, :string
      add :code, :string
      add :description, :text
      add :address, :text
      add :start_date, :string
      add :end_date, :text
      add :status, :integer

      add :client_id, references(:clients, on_delete: :nothing), null: false
      add :state_id, references(:states, on_delete: :nothing), null: false
      add :local_id, references(:locals, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:projects, [:client_id])
    create index(:projects, [:state_id])
    create index(:projects, [:local_id])

  end
end
