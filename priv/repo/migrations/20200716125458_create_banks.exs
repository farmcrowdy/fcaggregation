defmodule Src.Repo.Migrations.CreateBanks do
  use Ecto.Migration

  def change do
    create table(:banks) do
      add :bank_name, :string

      timestamps()
    end

  end
end
