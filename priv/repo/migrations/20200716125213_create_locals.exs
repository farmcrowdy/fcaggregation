defmodule Src.Repo.Migrations.CreateLocals do
  use Ecto.Migration

  def change do
    create table(:locals) do
      add :local_name, :string

      add :state_id, references(:states, on_delete: :nothing), null: false

      timestamps()
    end

      create index(:locals, [:state_id])
  end
end
