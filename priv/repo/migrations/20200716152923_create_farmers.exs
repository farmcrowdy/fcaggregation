defmodule Src.Repo.Migrations.CreateFarmers do
  use Ecto.Migration

  def change do
    create table(:fcdy_novus) do
      add :fname, :string
      add :sname, :string
      add :bvnNumber, :string
      add :idType, :string
      add :idNumber, :string
      add :farmerId, :string
      add :bankName, :string
      add :govtString, :string
      add :nextOfKin, :string
      add :synced, :string
      add :nextOfKinPhone, :string
      add :role, :string
      add :dob, :string
      add :dobForPaga, :string
      add :gender, :string
      add :f_print, :text
      add :phone, :string
      add :marital_status, :string
      add :crop_proficiency, :string
      add :income_range, :string
      add :pro_image, :text
      add :pro_image_thumbnail, :text
      add :comment, :text
      add :acct_number, :string
      add :land_area_farmed, :string
      add :lat1, :string
      add :lat2, :string
      add :lat3, :string
      add :lat4, :string
      add :haveSmartPhone, :string
      add :long1, :string
      add :long2, :string
      add :long3, :string
      add :long4, :string
      add :number_of_dependants, :integer
      add :years_of_experience, :integer
      add :coordinates, :string
      add :comments, :text
      add :timestamp, :string
      add :uploadedToParent, :string
      add :sourceOfData, :integer
      add :readWriteEnglish, :string
      add :hasFCMBWallet, :integer
      add :fcmb_pin, :integer, default: 0
      add :appVersion, :string

      add :user_add_id, references(:users, on_delete: :nothing), null: false
      add :project_id, references(:projects, on_delete: :nothing)
      add :client_id, references(:clients, on_delete: :nothing), null: false
      add :state_id, references(:states, on_delete: :nothing), null: false
      add :local_id, references(:locals, on_delete: :nothing), null: false
      add :farm_group_id, references(:farm_group, on_delete: :nothing), null: false
      add :farm_location_id, references(:farm_location, on_delete: :nothing), null: false
      add :bank_id, references(:banks, on_delete: :nothing), null: false

      timestamps()
    end

  end
end
