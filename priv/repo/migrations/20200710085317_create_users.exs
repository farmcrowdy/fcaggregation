defmodule Src.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :firstname, :string
      add :surname, :string
      add :email, :string
      add :password, :string
      add :uphone, :string
      add :dob, :utc_datetime, null: true
      add :approved, :integer, default: 1
      add :gender, :string
      add :role, :string
      add :pin, :integer, null: true, default: 0
      add :hasFCMBWallet, :integer, null: true, default: 0
      add :client_id, references(:clients, on_delete: :nothing), null: false
      add :access_level_id, references(:access_levels, on_delete: :nothing), null: false, default: 1


      timestamps()
    end

    create unique_index(:users, [:email])
    create index(:users, [:client_id])
    create index(:users, [:access_level_id])

  end
end
