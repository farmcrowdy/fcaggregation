defmodule Src.Repo.Migrations.CreateAccessLevels do
  use Ecto.Migration

  def change do
    create table(:access_levels) do
      add :name, :string

      timestamps()
    end

  end
end
