defmodule Src.Repo.Migrations.CreateFarmGroup do
  use Ecto.Migration

  def change do
    create table(:farm_group) do
      add :group_name, :string
      add :gr_crop_type, :string
      add :cycle_duration, :string
      add :land_area_assign, :integer
      add :land_area_unit, :string
      add :gr_location_id, references(:farm_location, on_delete: :nothing)

      timestamps()
    end

    create index(:farm_group, [:gr_location_id])

  end
end
