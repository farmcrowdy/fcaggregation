defmodule Src.Repo.Migrations.ChangeLocals do
  use Ecto.Migration

  def change do
    alter table(:locals) do
      remove :inserted_at
      remove :updated_at
    end
  end
end
