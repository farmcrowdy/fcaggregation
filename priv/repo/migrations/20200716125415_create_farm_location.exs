defmodule Src.Repo.Migrations.CreateFarmLocation do
  use Ecto.Migration

  def change do
    create table(:farm_location) do
      add :location, :string

      timestamps()
    end

  end
end
