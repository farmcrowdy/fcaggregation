defmodule Src.AccountsTest do
  use Src.DataCase

  alias Src.Accounts

  describe "clients" do
    alias Src.Accounts.Client

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def client_fixture(attrs \\ %{}) do
      {:ok, client} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_client()

      client
    end

    test "list_clients/0 returns all clients" do
      client = client_fixture()
      assert Accounts.list_clients() == [client]
    end

    test "get_client!/1 returns the client with given id" do
      client = client_fixture()
      assert Accounts.get_client!(client.id) == client
    end

    test "create_client/1 with valid data creates a client" do
      assert {:ok, %Client{} = client} = Accounts.create_client(@valid_attrs)
      assert client.age == 42
      assert client.name == "some name"
    end

    test "create_client/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_client(@invalid_attrs)
    end

    test "update_client/2 with valid data updates the client" do
      client = client_fixture()
      assert {:ok, %Client{} = client} = Accounts.update_client(client, @update_attrs)
      assert client.age == 43
      assert client.name == "some updated name"
    end

    test "update_client/2 with invalid data returns error changeset" do
      client = client_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_client(client, @invalid_attrs)
      assert client == Accounts.get_client!(client.id)
    end

    test "delete_client/1 deletes the client" do
      client = client_fixture()
      assert {:ok, %Client{}} = Accounts.delete_client(client)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_client!(client.id) end
    end

    test "change_client/1 returns a client changeset" do
      client = client_fixture()
      assert %Ecto.Changeset{} = Accounts.change_client(client)
    end
  end

  describe "access_levels" do
    alias Src.Accounts.Access_level

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def access_level_fixture(attrs \\ %{}) do
      {:ok, access_level} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_access_level()

      access_level
    end

    test "list_access_levels/0 returns all access_levels" do
      access_level = access_level_fixture()
      assert Accounts.list_access_levels() == [access_level]
    end

    test "get_access_level!/1 returns the access_level with given id" do
      access_level = access_level_fixture()
      assert Accounts.get_access_level!(access_level.id) == access_level
    end

    test "create_access_level/1 with valid data creates a access_level" do
      assert {:ok, %Access_level{} = access_level} = Accounts.create_access_level(@valid_attrs)
      assert access_level.age == 42
      assert access_level.name == "some name"
    end

    test "create_access_level/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_access_level(@invalid_attrs)
    end

    test "update_access_level/2 with valid data updates the access_level" do
      access_level = access_level_fixture()
      assert {:ok, %Access_level{} = access_level} = Accounts.update_access_level(access_level, @update_attrs)
      assert access_level.age == 43
      assert access_level.name == "some updated name"
    end

    test "update_access_level/2 with invalid data returns error changeset" do
      access_level = access_level_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_access_level(access_level, @invalid_attrs)
      assert access_level == Accounts.get_access_level!(access_level.id)
    end

    test "delete_access_level/1 deletes the access_level" do
      access_level = access_level_fixture()
      assert {:ok, %Access_level{}} = Accounts.delete_access_level(access_level)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_access_level!(access_level.id) end
    end

    test "change_access_level/1 returns a access_level changeset" do
      access_level = access_level_fixture()
      assert %Ecto.Changeset{} = Accounts.change_access_level(access_level)
    end
  end
end
