defmodule Src.FarmsTest do
  use Src.DataCase

  alias Src.Farms

  describe "farmers" do
    alias Src.Farms.Farmer

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def farmer_fixture(attrs \\ %{}) do
      {:ok, farmer} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Farms.create_farmer()

      farmer
    end

    test "list_farmers/0 returns all farmers" do
      farmer = farmer_fixture()
      assert Farms.list_farmers() == [farmer]
    end

    test "get_farmer!/1 returns the farmer with given id" do
      farmer = farmer_fixture()
      assert Farms.get_farmer!(farmer.id) == farmer
    end

    test "create_farmer/1 with valid data creates a farmer" do
      assert {:ok, %Farmer{} = farmer} = Farms.create_farmer(@valid_attrs)
      assert farmer.age == 42
      assert farmer.name == "some name"
    end

    test "create_farmer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Farms.create_farmer(@invalid_attrs)
    end

    test "update_farmer/2 with valid data updates the farmer" do
      farmer = farmer_fixture()
      assert {:ok, %Farmer{} = farmer} = Farms.update_farmer(farmer, @update_attrs)
      assert farmer.age == 43
      assert farmer.name == "some updated name"
    end

    test "update_farmer/2 with invalid data returns error changeset" do
      farmer = farmer_fixture()
      assert {:error, %Ecto.Changeset{}} = Farms.update_farmer(farmer, @invalid_attrs)
      assert farmer == Farms.get_farmer!(farmer.id)
    end

    test "delete_farmer/1 deletes the farmer" do
      farmer = farmer_fixture()
      assert {:ok, %Farmer{}} = Farms.delete_farmer(farmer)
      assert_raise Ecto.NoResultsError, fn -> Farms.get_farmer!(farmer.id) end
    end

    test "change_farmer/1 returns a farmer changeset" do
      farmer = farmer_fixture()
      assert %Ecto.Changeset{} = Farms.change_farmer(farmer)
    end
  end

  describe "projects" do
    alias Src.Farms.Project

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def project_fixture(attrs \\ %{}) do
      {:ok, project} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Farms.create_project()

      project
    end

    test "list_projects/0 returns all projects" do
      project = project_fixture()
      assert Farms.list_projects() == [project]
    end

    test "get_project!/1 returns the project with given id" do
      project = project_fixture()
      assert Farms.get_project!(project.id) == project
    end

    test "create_project/1 with valid data creates a project" do
      assert {:ok, %Project{} = project} = Farms.create_project(@valid_attrs)
      assert project.age == 42
      assert project.name == "some name"
    end

    test "create_project/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Farms.create_project(@invalid_attrs)
    end

    test "update_project/2 with valid data updates the project" do
      project = project_fixture()
      assert {:ok, %Project{} = project} = Farms.update_project(project, @update_attrs)
      assert project.age == 43
      assert project.name == "some updated name"
    end

    test "update_project/2 with invalid data returns error changeset" do
      project = project_fixture()
      assert {:error, %Ecto.Changeset{}} = Farms.update_project(project, @invalid_attrs)
      assert project == Farms.get_project!(project.id)
    end

    test "delete_project/1 deletes the project" do
      project = project_fixture()
      assert {:ok, %Project{}} = Farms.delete_project(project)
      assert_raise Ecto.NoResultsError, fn -> Farms.get_project!(project.id) end
    end

    test "change_project/1 returns a project changeset" do
      project = project_fixture()
      assert %Ecto.Changeset{} = Farms.change_project(project)
    end
  end

  describe "states" do
    alias Src.Farms.State

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def state_fixture(attrs \\ %{}) do
      {:ok, state} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Farms.create_state()

      state
    end

    test "list_states/0 returns all states" do
      state = state_fixture()
      assert Farms.list_states() == [state]
    end

    test "get_state!/1 returns the state with given id" do
      state = state_fixture()
      assert Farms.get_state!(state.id) == state
    end

    test "create_state/1 with valid data creates a state" do
      assert {:ok, %State{} = state} = Farms.create_state(@valid_attrs)
      assert state.age == 42
      assert state.name == "some name"
    end

    test "create_state/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Farms.create_state(@invalid_attrs)
    end

    test "update_state/2 with valid data updates the state" do
      state = state_fixture()
      assert {:ok, %State{} = state} = Farms.update_state(state, @update_attrs)
      assert state.age == 43
      assert state.name == "some updated name"
    end

    test "update_state/2 with invalid data returns error changeset" do
      state = state_fixture()
      assert {:error, %Ecto.Changeset{}} = Farms.update_state(state, @invalid_attrs)
      assert state == Farms.get_state!(state.id)
    end

    test "delete_state/1 deletes the state" do
      state = state_fixture()
      assert {:ok, %State{}} = Farms.delete_state(state)
      assert_raise Ecto.NoResultsError, fn -> Farms.get_state!(state.id) end
    end

    test "change_state/1 returns a state changeset" do
      state = state_fixture()
      assert %Ecto.Changeset{} = Farms.change_state(state)
    end
  end

  describe "locals" do
    alias Src.Farms.Local

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def local_fixture(attrs \\ %{}) do
      {:ok, local} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Farms.create_local()

      local
    end

    test "list_locals/0 returns all locals" do
      local = local_fixture()
      assert Farms.list_locals() == [local]
    end

    test "get_local!/1 returns the local with given id" do
      local = local_fixture()
      assert Farms.get_local!(local.id) == local
    end

    test "create_local/1 with valid data creates a local" do
      assert {:ok, %Local{} = local} = Farms.create_local(@valid_attrs)
      assert local.age == 42
      assert local.name == "some name"
    end

    test "create_local/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Farms.create_local(@invalid_attrs)
    end

    test "update_local/2 with valid data updates the local" do
      local = local_fixture()
      assert {:ok, %Local{} = local} = Farms.update_local(local, @update_attrs)
      assert local.age == 43
      assert local.name == "some updated name"
    end

    test "update_local/2 with invalid data returns error changeset" do
      local = local_fixture()
      assert {:error, %Ecto.Changeset{}} = Farms.update_local(local, @invalid_attrs)
      assert local == Farms.get_local!(local.id)
    end

    test "delete_local/1 deletes the local" do
      local = local_fixture()
      assert {:ok, %Local{}} = Farms.delete_local(local)
      assert_raise Ecto.NoResultsError, fn -> Farms.get_local!(local.id) end
    end

    test "change_local/1 returns a local changeset" do
      local = local_fixture()
      assert %Ecto.Changeset{} = Farms.change_local(local)
    end
  end

  describe "farm_group" do
    alias Src.Farms.Farmgroup

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def farmgroup_fixture(attrs \\ %{}) do
      {:ok, farmgroup} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Farms.create_farmgroup()

      farmgroup
    end

    test "list_farm_group/0 returns all farm_group" do
      farmgroup = farmgroup_fixture()
      assert Farms.list_farm_group() == [farmgroup]
    end

    test "get_farmgroup!/1 returns the farmgroup with given id" do
      farmgroup = farmgroup_fixture()
      assert Farms.get_farmgroup!(farmgroup.id) == farmgroup
    end

    test "create_farmgroup/1 with valid data creates a farmgroup" do
      assert {:ok, %Farmgroup{} = farmgroup} = Farms.create_farmgroup(@valid_attrs)
      assert farmgroup.age == 42
      assert farmgroup.name == "some name"
    end

    test "create_farmgroup/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Farms.create_farmgroup(@invalid_attrs)
    end

    test "update_farmgroup/2 with valid data updates the farmgroup" do
      farmgroup = farmgroup_fixture()
      assert {:ok, %Farmgroup{} = farmgroup} = Farms.update_farmgroup(farmgroup, @update_attrs)
      assert farmgroup.age == 43
      assert farmgroup.name == "some updated name"
    end

    test "update_farmgroup/2 with invalid data returns error changeset" do
      farmgroup = farmgroup_fixture()
      assert {:error, %Ecto.Changeset{}} = Farms.update_farmgroup(farmgroup, @invalid_attrs)
      assert farmgroup == Farms.get_farmgroup!(farmgroup.id)
    end

    test "delete_farmgroup/1 deletes the farmgroup" do
      farmgroup = farmgroup_fixture()
      assert {:ok, %Farmgroup{}} = Farms.delete_farmgroup(farmgroup)
      assert_raise Ecto.NoResultsError, fn -> Farms.get_farmgroup!(farmgroup.id) end
    end

    test "change_farmgroup/1 returns a farmgroup changeset" do
      farmgroup = farmgroup_fixture()
      assert %Ecto.Changeset{} = Farms.change_farmgroup(farmgroup)
    end
  end

  describe "farm_location" do
    alias Src.Farms.Farmlocation

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def farmlocation_fixture(attrs \\ %{}) do
      {:ok, farmlocation} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Farms.create_farmlocation()

      farmlocation
    end

    test "list_farm_location/0 returns all farm_location" do
      farmlocation = farmlocation_fixture()
      assert Farms.list_farm_location() == [farmlocation]
    end

    test "get_farmlocation!/1 returns the farmlocation with given id" do
      farmlocation = farmlocation_fixture()
      assert Farms.get_farmlocation!(farmlocation.id) == farmlocation
    end

    test "create_farmlocation/1 with valid data creates a farmlocation" do
      assert {:ok, %Farmlocation{} = farmlocation} = Farms.create_farmlocation(@valid_attrs)
      assert farmlocation.age == 42
      assert farmlocation.name == "some name"
    end

    test "create_farmlocation/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Farms.create_farmlocation(@invalid_attrs)
    end

    test "update_farmlocation/2 with valid data updates the farmlocation" do
      farmlocation = farmlocation_fixture()
      assert {:ok, %Farmlocation{} = farmlocation} = Farms.update_farmlocation(farmlocation, @update_attrs)
      assert farmlocation.age == 43
      assert farmlocation.name == "some updated name"
    end

    test "update_farmlocation/2 with invalid data returns error changeset" do
      farmlocation = farmlocation_fixture()
      assert {:error, %Ecto.Changeset{}} = Farms.update_farmlocation(farmlocation, @invalid_attrs)
      assert farmlocation == Farms.get_farmlocation!(farmlocation.id)
    end

    test "delete_farmlocation/1 deletes the farmlocation" do
      farmlocation = farmlocation_fixture()
      assert {:ok, %Farmlocation{}} = Farms.delete_farmlocation(farmlocation)
      assert_raise Ecto.NoResultsError, fn -> Farms.get_farmlocation!(farmlocation.id) end
    end

    test "change_farmlocation/1 returns a farmlocation changeset" do
      farmlocation = farmlocation_fixture()
      assert %Ecto.Changeset{} = Farms.change_farmlocation(farmlocation)
    end
  end

  describe "banks" do
    alias Src.Farms.Bank

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def bank_fixture(attrs \\ %{}) do
      {:ok, bank} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Farms.create_bank()

      bank
    end

    test "list_banks/0 returns all banks" do
      bank = bank_fixture()
      assert Farms.list_banks() == [bank]
    end

    test "get_bank!/1 returns the bank with given id" do
      bank = bank_fixture()
      assert Farms.get_bank!(bank.id) == bank
    end

    test "create_bank/1 with valid data creates a bank" do
      assert {:ok, %Bank{} = bank} = Farms.create_bank(@valid_attrs)
      assert bank.age == 42
      assert bank.name == "some name"
    end

    test "create_bank/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Farms.create_bank(@invalid_attrs)
    end

    test "update_bank/2 with valid data updates the bank" do
      bank = bank_fixture()
      assert {:ok, %Bank{} = bank} = Farms.update_bank(bank, @update_attrs)
      assert bank.age == 43
      assert bank.name == "some updated name"
    end

    test "update_bank/2 with invalid data returns error changeset" do
      bank = bank_fixture()
      assert {:error, %Ecto.Changeset{}} = Farms.update_bank(bank, @invalid_attrs)
      assert bank == Farms.get_bank!(bank.id)
    end

    test "delete_bank/1 deletes the bank" do
      bank = bank_fixture()
      assert {:ok, %Bank{}} = Farms.delete_bank(bank)
      assert_raise Ecto.NoResultsError, fn -> Farms.get_bank!(bank.id) end
    end

    test "change_bank/1 returns a bank changeset" do
      bank = bank_fixture()
      assert %Ecto.Changeset{} = Farms.change_bank(bank)
    end
  end
end
